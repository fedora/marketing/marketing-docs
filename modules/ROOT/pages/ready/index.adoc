include::ROOT:partial$attributes.adoc[]

= Introducing Fedora Ready
Fedora Ready is a program by the Fedora Project to support vendors who ship or support Fedora Linux on their hardware.
// Article content last reviewed and verified by a human on…
2024-07-31

[NOTE]
====
This article is draft content and is not yet finalized.
This notice will be removed once the Fedora Ready program is officially launched.
====

The Fedora Ready program supports vendors who meet our criteria in their support of Fedora Linux on their devices.
Also, we want to simplify the process for our users to find and purchase these supported devices.
Our xref:ready/list.adoc[central hub] lists all Fedora Ready devices so people can easily locate where to buy them.


[[fedora-ready-is-not]]
== What Fedora Ready is not

* Fedora Ready is NOT a certification or endorsement program. 
* Fedora Ready is not a formal partnership program nor are we in legal partnership with vendors listed on our compatibility list. 
  Vendors oversee the lifecycle of their devices and offer what makes a good experience for their customers.


[[criteria]]
== Criteria for Vendor Listing

Vendors are listed under Fedora Ready if:

* A vendor is selling a computer with Fedora Linux available pre-installed:
** We have reputable contact with decision-makers or points of contact within your team.
** The computer is available for purchase online.
** If the product is in the pipeline and not ready yet, then we should be in contact in the lead up to the product's launch to tie our promotional efforts to that moment.

* A vendor is officially supporting Fedora Linux as an operating system for their computer:
** We have reputable contact with decision-makers or points of contact within your team.
** You need to publicly share which devices have the support for Fedora Linux online.
** If the support is something that is planned, then we should be in contact with your team to tie promotion to the product's announcement.

* A vendor offers official Fedora Linux support without preinstallation.

* They agree to ship Fedora Workstation unmodified when using the Fedora trademark and are in compliance with all Fedora trademark guidelines.

[NOTE]
====
The criteria outlined here are subject to change as the program evolves. 
We expect to iterate and refine these criteria based on how vendors support Fedora Linux and as the program develops over time.
====


[[delisting]]
== Criteria for Delisting Vendor

Fedora Ready vendors are hardware companies that meet our criteria in their support of Fedora Linux on their hardware. 
To keep our program running smoothly, we occasionally re-evaluate vendor offerings for compliance. 
If a vendor's support of Fedora Linux on their device no longer align with our criteria, we may need to remove them from the list. 
This helps us maintain the program's value.

[[delisting-criteria]]
=== A Vendor will be delisted from the program if:

* A vendor decides to stop supporting Fedora Linux on their devices after confirmation.
* A vendor ships a modified Fedora  Linux when using the Fedora trademark or in a manner that differs from what we offer directly to our users.
* A vendor is non-compliance with all Fedora trademark guidelines.
* A vendor's actions conflict with our values or become detrimental to the program's integrity. 
In such cases, we reserve the right to take necessary actions, including removing the vendor and /or product from the program, to protect the integrity of the program and our trademark.

[[delisting-process]]
=== Delisting Process

*Initial Communication*:
If a vendor does not meet our criteria, we will notify the vendor about the possibility of being delisted. 
We will specify the issues observed and the criteria they have failed to meet.

*Vendor Response Period*:
The vendor will have a specified period (e.g., 30 days) to respond to the notification and implement the necessary corrective actions to comply with Fedora Ready standards.

*Evaluation of Response*:
After the response period, we will re-evaluate the vendor’s status. 
If the vendor has addressed the issues satisfactorily, we will continue their listing with a probationary review scheduled. 
If the vendor has failed to comply or the response is unsatisfactory, we will proceed to the next step.

*Final Decision and Notification*:
We will make a final decision on delisting the vendor. 
If delisting is decided, we will notify the vendor of the decision and the reasons for it.

*Update Listing*:
We will update the xref:ready/list.adoc[Fedora Ready compatibility list] accordingly.


[[limitations]]
== Understanding our Limitations

Our program is run by a dedicated team of volunteers, who are not full-time employees.
This means that we do not have full-time availability or dedicated company resources, but our commitment to supporting vendors remains unwavering.

[[vendors]]
=== What This Means for Vendors 

- *Availability:* 
  Our team members volunteer their time alongside their personal and professional commitments. 
  Because of this, our response times may be a bit longer than what you might expect from a full-time corporate team.

- *Resources:* 
  We do not have specific resources allocated exclusively for this program.
  However, we are here to provide feedback and guidance in various forms.
  Ultimately, it is up to the vendors to implement and support Fedora Linux on their devices.

[[past]]
=== How We've Supported Vendors in the Past

Despite these constraints, we have successfully provided support to vendors in various ways. 
Here are a few examples of what we have helped vendors with in the past:

* Two product launches for Slimbook which has included social media campaigns, distribution to media outlets, co-writing of the product's announcement, and an episode on the https://fedoraproject.fireside.fm/31[Fedora Podcast]
* Direct and confidential contact with the Fedora Quality Team for quick help with issues as they have arisen (not promised, case by case)
* The Fedora Ready xref:ready/list.adoc[compatibility list]
* Regular social media posts and boosts between accounts to support each other among our respective communities 


[[support]]
== How We Support Vendors

Fedora Ready support vendors by offering:

* Marketing support through Fedora's channels.
* Technical support for integrating Fedora Linux with their hardware.
* Liaison support to foster community connections.
* Curating feedback from users.


[[devices]]
== Vendor and Device Categories

* *Fedora preinstalled:* 
  Computers that come with Fedora Linux officially preinstalled by the vendor. 
* *Fedora supported by vendor:* 
  Computers tested and verified to run Fedora Linux successfully.
* *Fedora may run:* 
  Computers that may run Fedora Linux successfully based on informal assurances from the vendor. 

For example, some Lenovo devices have not been tested for Fedora Linux, but they were tested for Ubuntu.
The Lenovo Linux team has informally said that Fedora Linux has a decent chance of running on those devices. 


[[list]]
== Where Can You Find Fedora Ready Vendors and Devices?

You can find Vendors and Fedora Ready approved devices on our xref:ready/list.adoc#fedora-ready-approved-devices[compatibility list].


[[contact]]
== How to Contact Us

Vendors interested joining the Fedora Ready program can email us at fedora-ready-team [at] fedoraproject [dot] org.
Then, we will discuss how to get you onboard.


[[changelog]]
== Changelog

For added transparency about when changes are made to this document, a changelog is maintained below.

.Social Media Contribution Model changelog
|===
| Version # | Authors | Description

| 1.0
| Roseline Bassey; Joseph Gayoso; Justin W. Flory
| Initial release.
  Copyediting led by Bassey.
|===
